# compressed_tablebook_json


Example table_charter data generated with [crosstabser::Tabula$get_crosstabs_data()](https://urswilke.gitlab.io/crosstabser/reference/Tabula.html#method-Tabula-get_crosstabs_data) &
[crosstabser::gen_data_json()](https://urswilke.gitlab.io/crosstabser/reference/gen_data_json.html); see [here](https://urswilke.github.io/datadaptor-crosstabser-table_charter-demo/datadaptor-crosstabser-table_charter-demo.html#sec-calc-ct) for how that can be done.
